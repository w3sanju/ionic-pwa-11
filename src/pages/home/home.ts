import { Component,Injectable } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { Subscription} from 'rxjs/Subscription';

// import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from  '@angular/common/http';

@Injectable()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  connected: Subscription;
  disconnected: Subscription;

  networkMsg: any;

  apiStatus: any;
  imgPath: any;

  API_working = 'https://spreadsheets.google.com/feeds/list/1FBe6JAxPqgswL6iS3JjZ1sAeKDh_0BQpuOgf4aYzAQc/od6/public/values?alt=json';

  API_Path = 'https://spreadsheets.google.com/feeds/list/1FBe6JAxPqgswL6iS3JjZ1sAeKDh_0BQpuOgf4aYzAQc/od6/public/values?alt=json'



  constructor( private  httpClient:  HttpClient, private toast: ToastController, private network: Network, public navCtrl: NavController ) {


  }

  ngOnInit()
  {


    this.getExcelsheet().subscribe((data:  Array<object>) => {
        console.log(data);
        console.log(data['feed']['entry'].length);
        this.apiStatus = 'API calling dynamically: Got ' + data['feed']['entry'].length + ' records';
      },
      (err: HttpErrorResponse) => {
	  if (err.error instanceof Error) {
      this.apiStatus = 'An error occurred:', err.error.message;
		 console.log('An error occurred:', err.error.message);
	  } else {
      this.apiStatus = 'Backend returned status code: ', err.status;
      this.apiStatus = 'Response body:', err.error;
		 console.log('Backend returned status code: ', err.status);
		 console.log('Response body:', err.error);
	  }
       })
  }

  getExcelsheet(){
    // return  this.httpClient.get(`${this.API_URL}/users?since=100`);
    // return  this.httpClient.get(`https://spreadsheets.google.com/feeds/worksheets/1FBe6JAxPqgswL6iS3JjZ1sAeKDh_0BQpuOgf4aYzAQc/public/full?alt=json`);
    return  this.httpClient.get(`${this.API_working}`);
  }


  ionViewDidEnter() {

        // alert('ionViewDidEnter!');
    this.networkMsg = 'ionViewDidEnter!';

    // watch network for a disconnection
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.networkMsg = 'network was disconnected :-(';
      alert('network was disconnected :-(');
    });

    // stop disconnect watch
    disconnectSubscription.unsubscribe();


    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      this.networkMsg = 'network connected!';
      alert('network connected!');
      // We just got a connection but we need to wait briefly
       // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          this.networkMsg = 'we got a wifi connection, woohoo!';
        }
      }, 3000);
    });

    // stop connect watch
    connectSubscription.unsubscribe();


    this.connected = this.network.onConnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

    this.disconnected = this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

  }


  ionViewWillLeave(){
    this.connected.unsubscribe();
    this.disconnected.unsubscribe();
  }


  displayNetworkUpdate(connectionState: string){
    let networkType = this.network.type;
    if(connectionState == 'offline')
    {
      this.imgPath = 'https://via.placeholder.com/480x180.png/09f/fff?text=Yor+are+in+Offline+mode';
    }
    else
    {
      this.imgPath = 'http://lorempixel.com/400/200/';
    }
    this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present();
  }



}
